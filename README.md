# Instructions to using mass_spectrometry_mass_hunter_d_file_converter


## Install cwltool packages: https://github.com/common-workflow-language/cwltool

## Conversion workflow

![Mass spectrometry Mass hunter d File converter](msconvert_workflow.png)


## Step 1 : Conversion of RAW file to mzML format

Main file: msconvert_workflow_main.cwl

supplementary file: msconvert_workflow_file.yml

Execution-command: cwltool msconvert_workflow_main.cwl msconvert_workflow_file.yml

Two output Files will be produced:
- 1. mzML converted File
- 2. validation file (yml)

## Step 2:
 
Objective: To test the mzML file using FileInfo command from OpenMS

Main File: validation.cwl
supplementary file: output(yml) of Input 1 or any other files in same format.

command: cwltool validation.cwl <path of the output validation_file or files similar to its format>

output:
- test results in txt format.

