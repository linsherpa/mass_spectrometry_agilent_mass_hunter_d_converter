#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool
label: Extract the location/path of a file

requirements:
  InlineJavascriptRequirement: {}

inputs:
  input_file:
    type: Directory

outputs:
  output:
    type: string
  output2:
    type: string
  output3:
    type: string
  output4:
    type: string

expression: |
  ${
  var mainFolderPath= inputs.input_file.location.replace('file://','');
  var outputFolder= mainFolderPath.substr(0,mainFolderPath.lastIndexOf("/")) + '/converted_file';
  var mainFolder= mainFolderPath.substr(mainFolderPath.lastIndexOf("/") +1);
  mainFolder=mainFolder.substr(0,mainFolder.lastIndexOf('.'));
  var fullFileName= outputFolder+"/"+mainFolder+".mzML";
  return {"output": mainFolder, "output2": outputFolder, "output3": mainFolder, "output4": fullFileName}; }
