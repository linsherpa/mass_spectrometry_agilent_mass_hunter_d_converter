#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Test of msconvert output

baseCommand: ["FileInfo_anyuser", "-v"]
requirements:
  InlineJavascriptRequirement: {}
hints:
  DockerRequirement:
    dockerPull: lincoln1010/pwiz-skyline-i_agree_to_vendor_licenses

inputs:
  in_file:
    type: File
    inputBinding:
      prefix: -in

  in_dir:
    type: string

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.in_file.nameroot)_validation_result.txt

  output_dir:
    type: Directory
    outputBinding:
      glob: .
      outputEval: |
        ${
          self[0].basename = inputs.in_dir;
          return self[0]
        }



stdout: $(inputs.in_file.nameroot)_validation_result.txt

